
    C R Y P T O A N A L S

        "That thing which combines your Bittrex and 
         Kraken ledgers, into some PowerShell thing."

         AUTHOR: arfonzo, at gmail dot com.

    Ever want to monitor, manage and manipulate your cryptocurrency trades 
    and actions using PowerShell?

    Me too. Thus, cryptoanals was born. It's a set of PowerShell cmdlets that:

    - Combines and consolidates Bittrex and Kraken CSV exports into a single 
      consistent form. I don't use other exchanges but it would be trivial for
      you to add further support for <your_exchange_of_choice>.
    - Analyzes your activity.
    - Calculates your current balances based on trades.
    - Keeps it PowerShelly: your ledger, your wallet, your trades are all
      PowerShell objects, with the piping and data manipulation capabilities
      that PS can bring. 
      
    Still confused? Check some examples below.

Example usage:

- Get CSV exports from Kraken (ledger.csv, all options selected) and Bittrex (fullOrders.csv).
- Update $my_kraken and $my_bittrex to point to those files.
- Run the following in a PowerShell session:

    # Import the module. No other dependencies required.
    Import-Module -Force $home\path\to\cryptoanals.psm1

    # Update the ledger objects in your PowerShell session by importing the trades.
    # This calls Import-Trades, but you can also call it manually.
    Update-Ledger

    # We now have $kraken, $bittrex and $trades (a combination ofboth).
    # Display the ledger $trades.
    Show-Ledger $trades

    # Display the Kraken ledger, with GUI output.
    Show-Ledger $kraken -GUI

    # Pipe Bittrex trades to Format-Table for console display and manipulation.
    $bittrex | Format-Table

    # Run some analysis on your trades, and display wallet balances in GUI.
    Measure-Trades $trades -Wallet -GUI -Verbose -AveragePurchasePrice

    # Show only EGC trades, in a GUI by piping to Out-GridView.
    $trades|where { $_.buy_asset -eq "EGC" -OR $_.sell_asset -eq "EGC" }|Out-GridView

Enjoy!
- arfonzo