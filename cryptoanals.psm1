﻿<#

    C R Y P T O A N A L S

        "That thing which combines your Bittrex and 
         Kraken ledgers, into some PowerShell thing."

         AUTHOR: arfonzo, at gmail dot com.

    Ever want to monitor, manage and manipulate your cryptocurrency trades 
    and actions using PowerShell?

    Me too. Thus, cryptoanals was born. It's a set of PowerShell cmdlets that:

    - Combines and consolidates Bittrex and Kraken CSV exports into a single 
      consistent form. I don't use other exchanges but it would be trivial for
      you to add further support for <your_exchange_of_choice>.
    - Analyzes your activity.
    - Calculates your current balances based on trades.
    - Keeps it PowerShelly: your ledger, your wallet, your trades are all
      PowerShell objects, with the piping and data manipulation capabilities
      that PS can bring. 
      
    Still confused? Check some examples below.

Example usage:

- Get CSV exports from Kraken (ledger.csv, all options selected) and Bittrex (fullOrders.csv).
- Update $my_kraken and $my_bittrex to point to those files.
- Run the following in a PowerShell session:

    # Import the module. No other dependencies required.
    Import-Module -Force $home\path\to\cryptoanals.psm1

    # Update the ledger objects in your PowerShell session by importing the trades.
    # This calls Import-Trades, but you can also call it manually.
    Update-Ledger

    # We now have $kraken, $bittrex and $trades (a combination ofboth).
    # Display the ledger $trades.
    Show-Ledger $trades

    # Display the Kraken ledger, with GUI output.
    Show-Ledger $kraken -GUI

    # Pipe Bittrex trades to Format-Table for console display and manipulation.
    $bittrex | Format-Table

    # Run some analysis on your trades, and display wallet balances in GUI.
    Measure-Trades $trades -Wallet -GUI -Verbose -AveragePurchasePrice

    # Show only EGC trades, in a GUI by piping to Out-GridView.
    $trades|where { $_.buy_asset -eq "EGC" -OR $_.sell_asset -eq "EGC" }|Out-GridView

Enjoy!
- arfonzo
#>


$my_ledger = "$home\my-ledgers.csv";
$my_kraken = "$home\kraken-ledgers.csv";
$my_bittrex = "$home\bittrex-fullOrders.csv";

function Import-Trades {

    param (
        [switch]$Kraken,
        [switch]$Bittrex,
        [switch]$MyLedger,
        [string]$File
    )

    $trades = New-Object System.Collections.Generic.List[System.Object];

    <# Import and Export

    Export: use Export-Clixml

    Import: Import-Clixml. Cast back to System.Array type.

        [array]$test1 = Import-Clixml .\cryptotrades.clixml
        $test1.GetType()

        IsPublic IsSerial Name                                     BaseType
        -------- -------- ----                                     --------
        True     True     Object[]                                 System.Array
    #>

    if ($MyLedger) {
        <# Manual Ledger Entries
        #>
        $m = Import-Csv -Path $File;
        $unprocessed_trades = New-Object System.Collections.Generic.List[System.Object];      
        
        $m | ForEach-Object {
            <#
            $trade = New-Object –TypeName PSCustomObject
            $trade | Add-Member –MemberType NoteProperty –Name date –Value (Get-Date $_.date)
            $trade | Add-Member –MemberType NoteProperty –Name type –Value $_.type
            $trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value $_.sell_amount;
            $trade | Add-Member –MemberType NoteProperty –Name sell_asset –Value $_.asset;
            $trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value $_.buy_amount;
            $trade | Add-Member –MemberType NoteProperty –Name buy_asset –Value $_.buy_asset;
            $trade | Add-Member –MemberType NoteProperty –Name limit –Value $_.limit;
            $trade | Add-Member –MemberType NoteProperty –Name limit_asset –Value $_.limit_asset;
            $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value $_.fee
            $trade | Add-Member –MemberType NoteProperty –Name fee_asset –Value $_.asset;
            $trade | Add-Member –MemberType NoteProperty –Name exchange –Value "manual"
            $trade | Add-Member –MemberType NoteProperty –Name group –Value $null
            $trade | Add-Member –MemberType NoteProperty –Name comment –Value $null
            $trade | Add-Member –MemberType NoteProperty –Name id –Value $_.id
            #>

            $_.date = Get-Date $_.date
            $trades.Add($_);
        }      

        
    } elseif ($Kraken) {
        <#
         # KRAKEN
         #>

        $k = Import-Csv -Path $File;  
        $unprocessed_trades = New-Object System.Collections.Generic.List[System.Object];      
        
        $k |ForEach-Object {
            $trade = New-Object –TypeName PSCustomObject
            $trade | Add-Member –MemberType NoteProperty –Name date –Value (Get-Date $_.time)
            $trade | Add-Member –MemberType NoteProperty –Name type –Value $_.type

            if ([double] $_.amount -lt 0) {            
                # Selling half
                $trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value ([math]::Abs(([double]$_.amount)));
                $trade | Add-Member –MemberType NoteProperty –Name sell_asset –Value (ConvertKrakenCurrency($_.asset));
                
                $trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value $null
                $trade | Add-Member –MemberType NoteProperty –Name buy_asset –Value $null
            } else {
                # Buying half
                $trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value ([math]::Abs(([double]$_.amount)));
                $trade | Add-Member –MemberType NoteProperty –Name buy_asset –Value (ConvertKrakenCurrency($_.asset));
                
                $trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value $null              
                $trade | Add-Member –MemberType NoteProperty –Name sell_asset –Value $null                
            }

            <# Trade limit and asset #>
            $trade | Add-Member –MemberType NoteProperty –Name limit –Value $null;
            $trade | Add-Member –MemberType NoteProperty –Name limit_asset –Value $null;

            # Fees
            if ([double]$_.fee -gt 0) {
                
                $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value ([double]$_.fee)
                $trade | Add-Member –MemberType NoteProperty –Name fee_asset –Value (ConvertKrakenCurrency($_.asset));
            } else {
                
                $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value $null
                $trade | Add-Member –MemberType NoteProperty –Name fee_asset –Value $null
            }

            $trade | Add-Member –MemberType NoteProperty –Name exchange –Value "kraken"
            $trade | Add-Member –MemberType NoteProperty –Name group –Value $null
            $trade | Add-Member –MemberType NoteProperty –Name comment –Value $null
            $trade | Add-Member –MemberType NoteProperty –Name id –Value "$($_.refid)_$($_.txid)"
            
            $unprocessed_trades.Add($trade);
        }

        <# Combine trades with the same refid #>
        $i = 0;
        while ($i -lt $unprocessed_trades.Count) {
            Write-Debug "checking trade $($i+1) of $($unprocessed_trades.Count)"

            $skip = $false

            # Only check 'trade' types.
            if ($unprocessed_trades[$i].type -like "trade*") {
                Write-Debug "Trade found: $($unprocessed_trades[$i].id). Trying to find pair..."
                # Find trade pairs by refid.
                $tr = $unprocessed_trades[$i]
                $pair = $unprocessed_trades[$i+1]

                Write-Debug "$( ([string]$tr.id).Split('_')[0] ) -VS- $( ([string]$pair.id).Split('_')[0] )"

                if (([string]$tr.id).Split('_')[0] -eq ([string]$pair.id).Split('_')[0]) {
                    Write-Debug "Pair found: $($pair.id)"
                    $skip = $true

                    # Update properties.

                    if ($tr.buy_asset -eq $null) {
                        Write-Debug "buy_asset is empty, we should take: $($pair.buy_asset)"
                        $tr.buy_asset = $pair.buy_asset
                    } 
                    
                    if ($tr.buy_amount -eq $null) {
                        Write-Debug "buy_amount is empty, we should take: $($pair.buy_amount)"
                        $tr.buy_amount = $pair.buy_amount
                    } 
                    
                    if ($tr.sell_asset -eq $null) {
                        Write-Debug "sell_asset is empty, we should take: $($pair.sell_asset)"
                        $tr.sell_asset = $pair.sell_asset
                    } 
                    
                    if ($tr.sell_amount -eq $null) {
                        Write-Debug "sell_amount is empty, we should take: $($pair.sell_amount)"
                        $tr.sell_amount = $pair.sell_amount
                    }

                    if ($tr.fee_asset -eq $null) {
                        Write-Debug "fee_asset is empty, we should take: $($pair.fee_asset)"
                        $tr.fee_asset = $pair.fee_asset
                    } 
                    
                    if ($tr.fee_amount -eq $null) {
                        Write-Debug "fee_amount is empty, we should take: $($pair.fee_amount)"
                        $tr.fee_amount = $pair.fee_amount
                    }

                    # Note: the below is not needed if previously balanced. Kraken ledgers do not include fee in the price.
                    # Sell: The fee_amount needs to be reduced from the buy_amount.
                    # Buy: The fee_amount needs to be reduced from the sell_amount.                        
                    <#
                    if ($tr.fee_asset -eq $tr.buy_asset) {
                        $tr.buy_amount -= $tr.fee_amount
                    } else {
                        $tr.sell_amount -= $tr.fee_amount
                    }
                    #>

                    if ($tr.limit -eq $null) {
                        if ($pair.sell_amount -ne $null) {
                            # Kraken Buy order
                            $tr.type = "trade:buy"
                            
                            # Calculate limit
                            $tr.limit = [double]($tr.sell_amount/$tr.buy_amount)
                            $tr.limit_asset = $tr.sell_asset
                        } else {
                            # Kraken Sell order
                            $tr.type = "trade:sell"

                            # Calculate limit
                            $tr.limit = [double]($tr.buy_amount/$tr.sell_amount)
                            $tr.limit_asset = $tr.buy_asset
                        }
                        
                    }                    


                    Write-Debug "New obj: $($tr.sell_asset)-$($tr.buy_asset): $($tr.buy_amount) for $($tr.sell_amount)"

                    $trades.Add($tr);
                    
                } else {
                    Write-Warning "Warning: matching pair for trade could not be found. Passing through unpaired."
                    $trades.Add($unprocessed_trades[$i]);
                }


            } else {
                # For non-trades, we pass straight to the processed list.
                $trades.Add($unprocessed_trades[$i]);
            }


           if ($skip) {
                # Skip next item in list if it was paired.
                $i+=2
            } else {
                $i++;
            }
        }
        
    } elseif ($Bittrex) {
        <#
         # BITTREX
         #>

        #$b = Import-Csv -Path $File -Encoding Unicode;
        $b = Import-Csv -Path $File;
        $unprocessed_trades = New-Object System.Collections.Generic.List[System.Object];   
        
        $b |ForEach-Object {
            $trade = New-Object –TypeName PSCustomObject
            $trade | Add-Member –MemberType NoteProperty –Name date –Value ([datetime]$_.Closed)
            $type = $_.Type
            if ($_.Type -eq "LIMIT_BUY") {
                $type = "trade:buy"
                $pair = $_.Exchange.Split('-')

                $trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value ([double]$_.Quantity);
                $trade | Add-Member –MemberType NoteProperty –Name buy_asset –Value $pair[1];                
                                
                $trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value ([double]$_.Price);
                #$trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value ([float]($_.Price - $_.CommissionPaid));# Bittrex: take subtract CommissionPaid from Price
                $trade | Add-Member –MemberType NoteProperty –Name sell_asset –Value $pair[0];

                $trade | Add-Member –MemberType NoteProperty –Name limit –Value ([double]$_.Limit); 
                $trade | Add-Member –MemberType NoteProperty –Name limit_asset –Value $pair[0]; # Buy -> sell_asset
                
                $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value ([double]$_.CommissionPaid);
                $trade | Add-Member –MemberType NoteProperty –Name fee_asset –Value $pair[0]; # Bittrex: Fees always in BTC
                

            } elseif ($_.Type -eq "LIMIT_SELL") {
                $type = "trade:sell"
                $pair = $_.Exchange.Split('-')

                $trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value ([double]$_.Price);
                #$trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value ([float]($_.Price - $_.CommissionPaid));# Bittrex: take subtract CommissionPaid from Price
                $trade | Add-Member –MemberType NoteProperty –Name buy_asset –Value $pair[0];

                $trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value ([double]$_.Quantity);
                $trade | Add-Member –MemberType NoteProperty –Name sell_asset –Value $pair[1];

                $trade | Add-Member –MemberType NoteProperty –Name limit –Value ([double]$_.Limit);
                $trade | Add-Member –MemberType NoteProperty –Name limit_asset –Value $pair[0]; # Sell -> buy_asset
                
                $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value ([double]$_.CommissionPaid);
                $trade | Add-Member –MemberType NoteProperty –Name fee_asset –Value $pair[0]; # Bittrex: Fees always in BTC

            } elseif ($_.Type -eq "withdrawal") {
                $type = "withdrawal"
                $pair = $_.Exchange.Split('-')

                $trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value $null
                $trade | Add-Member –MemberType NoteProperty –Name buy_asset –Value $null
                
                $trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value ([double]$_.Quantity);
                $trade | Add-Member –MemberType NoteProperty –Name sell_asset –Value $pair[1];                
                
                <# tx flat rate fees for bittrex
                    0.2 EGC
                    0.2 GAME
                    0.1 SC
                    0.01 XLM
                    0.001 BCH
                    0.001 BTC
                    2 DOGE                                  
                #>
                if ($pair[1] -eq "EGC" -or $pair[1] -eq "GAME") {
                    $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value (0.2);
                } elseif ($pair[1] -eq "SC") {
                    $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value (0.1);
                } elseif ($pair[1] -eq "XLM") {
                    $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value (0.01);
                } elseif ($pair[1] -eq "BTC" -or $pair[1] -eq "BCH") {
                    $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value (0.001);
                } elseif ($pair[1] -eq "DOGE") {
                    $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value (2);
                } else {
                    $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value ([double]$_.CommissionPaid);
                }

                $trade | Add-Member –MemberType NoteProperty –Name fee_asset –Value $pair[1]; # Bittrex: Withdraw fees in $pair[1] currency

            } elseif ($_.Type -eq "deposit") {
                $type = "deposit"
                $pair = $_.Exchange.Split('-')

                $trade | Add-Member –MemberType NoteProperty –Name buy_amount –Value ([double]$_.Quantity);
                $trade | Add-Member –MemberType NoteProperty –Name buy_asset –Value $pair[0];
                
                $trade | Add-Member –MemberType NoteProperty –Name sell_amount –Value $null;
                $trade | Add-Member –MemberType NoteProperty –Name sell_asset –Value $null;
                
                $trade | Add-Member –MemberType NoteProperty –Name fee_amount –Value ([double]$_.CommissionPaid);
                $trade | Add-Member –MemberType NoteProperty –Name fee_asset –Value $pair[1]; # Bittrex: Deposit fees in coin?
                

            } else {
                Write-Error "I don't understand this type: $($_.Type)"
            }
            $trade | Add-Member –MemberType NoteProperty –Name type –Value $type
            $trade | Add-Member –MemberType NoteProperty –Name exchange –Value "bittrex"
            $trade | Add-Member –MemberType NoteProperty –Name group –Value $null
            $trade | Add-Member –MemberType NoteProperty –Name comment –Value $null
            $trade | Add-Member –MemberType NoteProperty –Name id –Value $_.OrderUuid
            
            $unprocessed_trades.Add($trade);
        }

        $trades = $unprocessed_trades;
        
    }

    return $trades;

}

<#

Measure-Trades
--------------

USAGE EXAMPLE:
    Import-Module -Verbose -Force Ccryptoanals.psm1
    $kraken = Import-Trades -Kraken .\ledgers.csv; $bittrex = Import-Trades -Bittrex .\art-fullOrders.csv; $trades = $kraken + $bittrex|sort date
    $eth_trades = $trades|where {$_.buy_asset -EQ "ETH" -or $_.sell_asset -EQ "ETH"}
    Measure-Trades $eth_trades -Balance -Verbose
#>
function Measure-Trades {
    param (
        [System.Collections.Generic.List[System.Object]]$TradesObject = $null,
        [switch]$AveragePurchasePrice,
        [switch]$GUI,
        [switch]$Wallet,
        [switch]$Verbose
    )

    if ($TradesObject -eq $null) {
        Write-Error "I need a -TradesObject from the output of Import-Trades.";
        return;
    }

    Write-Host "Analyzing trades..."

    # Fiat
    $wallet_EUR = @(0, "EUR", "euro");

    # Cryptos
    $wallet_BCH = @(0, "BCH", "bitcoin cash");
    $wallet_BTC = @(0, "BTC", "bitcoin");
    $wallet_DASH = @(0, "DASH", "euro");
    $wallet_EGC = @(0, "EGC", "evergreencoin");
    $wallet_ETC = @(0, "ETC", "ethereum classic");
    $wallet_ETH = @(0, "ETH", "ethereum");        
    $wallet_GAME = @(0, "GAME", "gamecredits");
    $wallet_ICN = @(0, "ICN", "iconomi");
    $wallet_MLN = @(0, "MLN", "melon");
    $wallet_NEO = @(0, "NEO", "neo");
    $wallet_REP = @(0, "REP", "augur (rep)");
    $wallet_SC = @(0, "SC", "siacoin");
    $wallet_UBQ = @(0, "UBQ", "ubiq");
    $wallet_DOGE = @(0, "DOGE", "dogecoin");
    $wallet_XLM = @(0, "XLM", "stellar lumens");
    $wallet_XMR = @(0, "XMR", "monero");
    $wallet_XRP = @(0, "XRP", "ripple");
    $wallet_ZEC = @(0, "ZEC", "zcash");
    
    $wallets = @(
        $wallet_EUR,
        $wallet_BCH,
        $wallet_BTC,
        $wallet_DASH,
        $wallet_EGC,
        $wallet_ETC,
        $wallet_ETH,
        $wallet_GAME,
        $wallet_ICN,
        $wallet_MLN,
        $wallet_NEO,
        $wallet_REP,
        $wallet_SC,
        $wallet_UBQ,
        $wallet_DOGE,
        $wallet_XLM,
        $wallet_XMR,
        $wallet_XRP,
        $wallet_ZEC
    )   

    $wallet_threshold = 0.00001

    if ($AveragePurchasePrice) {
        # Initialize objects for APP calculations
        $app_thingy = New-Object System.Collections.Generic.List[System.Object];
    }
    
    if ($Verbose) {Write-Host "$($TradesObject.Count) trades found.";}

    $TradesObject | ForEach-Object {        
        if ($_.type -eq "deposit") {
            # Deposits            
            
            if ($Verbose) {
                Write-Host "Deposit $($_.date): $($_.id)." -ForegroundColor Green
                Write-Host "`t$($_.buy_amount) $($_.buy_asset)." -ForegroundColor DarkGreen
            }

            $trade = $_
            switch ($_.buy_asset) {
                "EUR" {
                    $wallet_EUR[0] += $trade.buy_amount;
                }
                "BCH" {
                    $wallet_BCH[0] += $trade.buy_amount;
                }
                "BTC" {
                    $wallet_BTC[0] += $trade.buy_amount;
                }
                "DASH" {
                    $wallet_DASH[0] += $trade.buy_amount;
                }
                "EGC" {
                    $wallet_EGC[0] += $trade.buy_amount;
                }
                "ETC" {
                    $wallet_ETC[0] += $trade.buy_amount;
                }
                "ETH" {
                    $wallet_ETH[0] += $trade.buy_amount;
                }
                "GAME" {
                    $wallet_GAME[0] += $trade.buy_amount;
                }
                "ICN" {
                    $wallet_ICN[0] += $trade.buy_amount;
                }
                "MLN" {
                    $wallet_MLN[0] += $trade.buy_amount;
                }
                "NEO" {
                    $wallet_NEO[0] += $trade.buy_amount;
                }
                "REP" {
                    $wallet_REP[0] += $trade.buy_amount;
                }
                "SC" {
                    $wallet_SC[0] += $trade.buy_amount;
                }
                "UBQ" {
                    $wallet_UBQ[0] += $trade.buy_amount;
                }
                "DOGE" {
                    $wallet_DOGE[0] += $trade.buy_amount;
                }
                "XLM" {
                    $wallet_XLM[0] += $trade.buy_amount;
                }
                "XMR" {
                    $wallet_XMR[0] += $trade.buy_amount;
                }
                "XRP" {
                    $wallet_XRP[0] += $trade.buy_amount;
                }
                "ZEC" {
                    $wallet_ZEC[0] += $trade.buy_amount;
                }
                default {
                    Write-Host "TODO: DEPOSIT: $($trade.buy_asset)" -ForegroundColor Yellow
                }
            }

        } elseif ($_.type -eq "withdrawal") {
            # Withdrawals

            if ($Verbose) {
                Write-Host "Withdrawal $($_.date): $($_.id)" -ForegroundColor Red
                Write-Host "`t$($_.sell_amount) $($_.sell_asset)" -ForegroundColor DarkRed
            }
            $trade = $_
            switch ($_.sell_asset) {
                "EUR" {
                    $wallet_EUR[0] -= $trade.sell_amount;
                }
                "BCH" {
                    $wallet_BCH[0] -= $trade.sell_amount;
                }
                "BTC" {
                    $wallet_BTC[0] -= $trade.sell_amount;
                }
                "DASH" {
                    $wallet_DASH[0] -= $trade.sell_amount;
                }
                "EGC" {
                    $wallet_EGC[0] -= $trade.sell_amount;
                }
                "ETC" {
                    $wallet_ETC[0] -= $trade.sell_amount;
                }
                "ETH" {
                    $wallet_ETH[0] -= $trade.sell_amount;
                }
                "GAME" {
                    $wallet_GAME[0] -= $trade.sell_amount;
                }
                "ICN" {
                    $wallet_ICN[0] -= $trade.sell_amount;
                }
                "MLN" {
                    $wallet_MLN[0] -= $trade.sell_amount;
                }
                "NEO" {
                    $wallet_NEO[0] -= $trade.sell_amount;
                }
                "REP" {
                    $wallet_REP[0] -= $trade.sell_amount;
                }
                "SC" {
                    $wallet_SC[0] -= $trade.sell_amount;
                }
                "UBQ" {
                    $wallet_UBQ[0] -= $trade.sell_amount;
                }
                "DOGE" {
                    $wallet_DOGE[0] -= $trade.sell_amount;
                }
                "XLM" {
                    $wallet_XLM[0] -= $trade.sell_amount;
                }
                "XMR" {
                    $wallet_XMR[0] -= $trade.sell_amount;
                }
                "XRP" {
                    $wallet_XRP[0] -= $trade.sell_amount;
                }
                "ZEC" {
                    $wallet_ZEC[0] -= $trade.sell_amount;
                }

                default {
                    Write-Host "TODO: WITHDRAWAL: $($trade.sell_asset)" -ForegroundColor Yellow
                }
            }
        } elseif ($_.type -eq "transfer") {
            
            if ($_.buy_amount -gt 0) {
                # Transfers in
                if ($Verbose) {
                    Write-Host "Transfer in $($_.date): $($_.id)" -ForegroundColor Green
                    Write-Host "`t$($_.buy_amount) $($_.buy_asset)" -ForegroundColor DarkGreen
                }

                $trade = $_
                switch ($_.buy_asset) {
                    "EUR" {
                        $wallet_EUR[0] += $trade.buy_amount;
                    }
                    "BCH" {
                        $wallet_BCH[0] += $trade.buy_amount;
                    }
                    "BTC" {
                        $wallet_BTC[0] += $trade.buy_amount;
                    }
                    "DASH" {
                        $wallet_DASH[0] += $trade.buy_amount;
                    }
                    "EGC" {
                        $wallet_EGC[0] += $trade.buy_amount;
                    }
                    "ETC" {
                        $wallet_ETC[0] += $trade.buy_amount;
                    }
                    "ETH" {
                        $wallet_ETH[0] += $trade.buy_amount;
                    }
                    "GAME" {
                        $wallet_GAME[0] += $trade.buy_amount;
                    }
                    "ICN" {
                        $wallet_ICN[0] += $trade.buy_amount;
                    }
                    "MLN" {
                        $wallet_MLN[0] += $trade.buy_amount;
                    }
                    "NEO" {
                        $wallet_NEO[0] += $trade.buy_amount;
                    }
                    "REP" {
                        $wallet_REP[0] += $trade.buy_amount;
                    }
                    "SC" {
                        $wallet_SC[0] += $trade.buy_amount;
                    }
                    "UBQ" {
                        $wallet_UBQ[0] += $trade.buy_amount;
                    }
                    "DOGE" {
                        $wallet_DOGE[0] += $trade.buy_amount;
                    }
                    "XLM" {
                        $wallet_XLM[0] += $trade.buy_amount;
                    }
                    "XMR" {
                        $wallet_XMR[0] += $trade.buy_amount;
                    }
                    "XRP" {
                        $wallet_XRP[0] += $trade.buy_amount;
                    }
                    "ZEC" {
                        $wallet_ZEC[0] += $trade.buy_amount;
                    }

                    default {
                        Write-Error "TODO: TRANSFER IN: $($trade.buy_asset)" -ForegroundColor Yellow
                    }
                }

                
            } else {
                # Transfers out
                Write-Error "TODO: Transfer out: $($_.sell_amount) of $($_.sell_asset)" -ForegroundColor Yellow
            }

            

        } elseif ($_.type -like "trade*") {
            # Trades
            if ($Verbose) {
                Write-Host "Trade $($_.date): $($_.id)" -ForegroundColor Cyan
                
                $buycolor = [consolecolor]::DarkGreen
                $sellcolor = [consolecolor]::DarkRed

                if ($_.type -eq "trade:buy") {
                    $buycolor = [consolecolor]::Green
                } elseif ($_.type -eq "trade:sell") {
                    $sellcolor = [consolecolor]::Red
                }
                
                Write-Host "`tBuy $($_.buy_amount) $($_.buy_asset)" -ForegroundColor $buycolor
                Write-Host "`tSell $($_.sell_amount) $($_.sell_asset)" -ForegroundColor $sellcolor
            }

            if ($_.buy_amount -ne $null) {
                $trade = $_
                switch ($_.buy_asset) {
                    "EUR" {
                        $wallet_EUR[0] += $trade.buy_amount;
                    }
                    "BCH" {
                        $wallet_BCH[0] += $trade.buy_amount;
                    }
                    "BTC" {
                        $wallet_BTC[0] += $trade.buy_amount;
                    }
                    "DASH" {
                        $wallet_DASH[0] += $trade.buy_amount;
                    }
                    "EGC" {
                        $wallet_EGC[0] += $trade.buy_amount;
                    }
                    "ETC" {
                        $wallet_ETC[0] += $trade.buy_amount;
                    }
                    "ETH" {
                        $wallet_ETH[0] += $trade.buy_amount;
                    }
                    "GAME" {
                        $wallet_GAME[0] += $trade.buy_amount;
                    }
                    "ICN" {
                        $wallet_ICN[0] += $trade.buy_amount;
                    }
                    "MLN" {
                        $wallet_MLN[0] += $trade.buy_amount;
                    }
                    "NEO" {
                        $wallet_NEO[0] += $trade.buy_amount;
                    }
                    "REP" {
                        $wallet_REP[0] += $trade.buy_amount;
                    }
                    "SC" {
                        $wallet_SC[0] += $trade.buy_amount;
                    }
                    "UBQ" {
                        $wallet_UBQ[0] += $trade.buy_amount;
                    }
                    "DOGE" {
                        $wallet_DOGE[0] += $trade.buy_amount;
                    }
                    "XLM" {
                        $wallet_XLM[0] += $trade.buy_amount;
                    }
                    "XMR" {
                        $wallet_XMR[0] += $trade.buy_amount;
                    }
                    "XRP" {
                        $wallet_XRP[0] += $trade.buy_amount;
                    }
                    "ZEC" {
                        $wallet_ZEC[0] += $trade.buy_amount;
                    }

                    default {
                        Write-Host "TODO: TRADES-BUY: $($trade.buy_asset)" -ForegroundColor Yellow
                    }
                } # end switch
            } # end if()

            if ($_.sell_amount -ne $null) {
                $trade = $_
                switch ($_.sell_asset) {
                    "EUR" {
                        $wallet_EUR[0] -= $trade.sell_amount;
                    }
                    "BCH" {
                        $wallet_BCH[0] -= $trade.sell_amount;
                    }
                    "BTC" {
                        $wallet_BTC[0] -= $trade.sell_amount;
                    }
                    "DASH" {
                        $wallet_DASH[0] -= $trade.sell_amount;
                    }
                    "EGC" {
                        $wallet_EGC[0] -= $trade.sell_amount;
                    }
                    "ETC" {
                        $wallet_ETC[0] -= $trade.sell_amount;
                    }
                    "ETH" {
                        $wallet_ETH[0] -= $trade.sell_amount;
                    }
                    "GAME" {
                        $wallet_GAME[0] -= $trade.sell_amount;
                    }
                    "ICN" {
                        $wallet_ICN[0] -= $trade.sell_amount;
                    }
                    "MLN" {
                        $wallet_MLN[0] -= $trade.sell_amount;
                    }
                    "NEO" {
                        $wallet_NEO[0] -= $trade.sell_amount;
                    }
                    "REP" {
                        $wallet_REP[0] -= $trade.sell_amount;
                    }
                    "SC" {
                        $wallet_SC[0] -= $trade.sell_amount;
                    }
                    "UBQ" {
                        $wallet_UBQ[0] -= $trade.sell_amount;
                    }
                    "DOGE" {
                        $wallet_DOGE[0] -= $trade.sell_amount;
                    }
                    "XLM" {
                        $wallet_XLM[0] -= $trade.sell_amount;
                    }
                    "XMR" {
                        $wallet_XMR[0] -= $trade.sell_amount;
                    }
                    "XRP" {
                        $wallet_XRP[0] -= $trade.sell_amount;
                    }
                    "ZEC" {
                        $wallet_ZEC[0] -= $trade.sell_amount;
                    }

                    default {
                        Write-Host "TODO: TRADES-SELL: $($trade.sell_asset)" -ForegroundColor Yellow
                    }
                } # end switch
            } # end if()
        # end if type trade
        } else {
            Write-Host "Type not matched: $($_.type)" -ForegroundColor Yellow
        }

        # For all types, check fees
        if ($_.fee_amount -gt 0) {
            if ($Verbose) {
                Write-Host "`tFee: $('{0:N8}' -f $_.fee_amount) $($_.fee_asset)" -ForegroundColor DarkRed;
            }

            $trade = $_
            switch ($_.fee_asset) {
                "EUR" {
                    $wallet_EUR[0] -= $trade.fee_amount;
                }
                "BCH" {
                    $wallet_BCH[0] -= $trade.fee_amount;
                }
                "BTC" {
                    $wallet_BTC[0] -= $trade.fee_amount;
                }
                "DASH" {
                    $wallet_DASH[0] -= $trade.fee_amount;
                }
                "EGC" {
                    $wallet_EGC[0] -= $trade.fee_amount;
                }
                "ETC" {
                    $wallet_ETC[0] -= $trade.fee_amount;
                }
                "ETH" {
                    $wallet_ETH[0] -= $trade.fee_amount;
                }
                "GAME" {
                    $wallet_GAME[0] -= $trade.fee_amount;
                }
                "ICN" {
                    $wallet_ICN[0] -= $trade.fee_amount;
                }
                "MLN" {
                    $wallet_MLN[0] -= $trade.fee_amount;
                }
                "NEO" {
                    $wallet_NEO[0] -= $trade.fee_amount;
                }
                "REP" {
                    $wallet_REP[0] -= $trade.fee_amount;
                }
                "SC" {
                    $wallet_SC[0] -= $trade.fee_amount;
                }
                "UBQ" {
                    $wallet_UBQ[0] -= $trade.fee_amount;
                }                
                "DOGE" {
                    $wallet_DOGE[0] -= $trade.fee_amount;
                }
                "XLM" {
                    $wallet_XLM[0] -= $trade.fee_amount;
                }
                "XMR" {
                    $wallet_XMR[0] -= $trade.fee_amount;
                }
                "XRP" {
                    $wallet_XRP[0] -= $trade.fee_amount;
                }
                "ZEC" {
                    $wallet_ZEC[0] -= $trade.fee_amount;
                }
                default {
                    Write-Host "TODO: FEES: $($trade.fee_asset)" -ForegroundColor Red -BackgroundColor DarkRed
                }
            } # end switch
        } # end fees

        # For all types, calculate APP
        if ($AveragePurchasePrice) {
            if ($_.type -like "trade*") {

                if ($_.type -eq "trade:buy" ) {
                    # For each trade, obtain:
                    # - Price per unit (limit)
                    # - number of units (buy_amount)
                    # - Pair (sell_asset)
                    Write-Host "`t Buy $($_.buy_amount) $($_.buy_asset) " -ForegroundColor Green -BackgroundColor DarkGreen -NoNewline
                    Write-Host " @ $("{0:N8}" -f ($_.limit)) $($_.limit_asset) / 1 $($_.buy_asset)"
                } elseif ($_.type -eq "trade:sell") {
                    Write-Host "`t Sell $($_.sell_amount) $($_.sell_asset) " -ForegroundColor Red -BackgroundColor DarkRed -NoNewline
                    Write-Host " @ $("{0:N8}" -f ($_.limit)) $($_.limit_asset) / 1 $($_.sell_asset)"
                }
            }
        }
    }

    
    if ($Wallet) {
        # Wallet Balances

        $walletsObj= New-Object System.Collections.Generic.List[System.Object];      

        Write-Host "`nWALLET(S)`n" -ForegroundColor Magenta    
        $wallets| ForEach-Object {
            $color = [ConsoleColor]::White

            if ($_[0] -le $wallet_threshold) {
                $color = [ConsoleColor]::DarkGray
            }

            #Write-Host "$($_[1])`t`t$($_[0])" -ForegroundColor $color
            Write-Host "$("{0,20:N8} {1,-4}" -f $_[0], $_[1])" -ForegroundColor $color

            # Don't show 0 balance wallets, or too small amounts.
            if ($_[0] -ge $wallet_threshold) {
                $wal = New-Object –TypeName PSCustomObject
                $wal | Add-Member –MemberType NoteProperty –Name amount –Value ("{0:N8}" -f $_[0])
                $wal | Add-Member –MemberType NoteProperty –Name asset –Value $_[1]
                $walletsObj.Add($wal);
            }

            
        }

        if ($GUI) {
            $walletsObj | Out-GridView -Title "cryptoanals: ur wallet";
        }

        # Update global object
        #$global:wallets = $walletsObj;
        
    }
}

function ConvertKrakenCurrency ([string]$cur) {
    switch ($cur) {
        "ZEUR" {
            return "EUR"
        }
        #"BCH" {
        #    return "BCH"
        #}
        "XXBT" {
            return "BTC"
        }
        "XETH" {
            return "ETH"
        }
        #"DASH" {
        #    return "DASH"
        #}
        "XETC" {
            return "ETC"
        }
        "XICN" {
            return "ICN"
        }
        "XMLN" {
            return "MLN"
        }
        "XREP" {
            return "REP"
        }
        "XXDG" {
            return "DOGE"
        }
        "XXLM" {
            return "XLM"
        }
        "XXMR" {
            return "XMR"
        }
        "XXRP" {
            return "XRP"
        }
        "XZEC" {
            return "ZEC"
        }

        default {
            #Write-Error "CURRENCY CONVERSION NOT FOUND FOR $cur"
            return $cur
        }
    }
}

function Show-Ledger {
    param (
        [System.Collections.Generic.List[System.Object]]$LedgerObject,
        [switch]$GUI
    )

    if ($LedgerObject -eq $null) {
        Write-Error "You need to pass me a '-LedgerObject <obj>' from the output of Import-Trades."
        return;
    }

    if ($GUI) {
        $LedgerObject | Select-Object date, type, @{Label="buy_amount"; Expression={"{0,16:N4}" -f $_.buy_amount}}, buy_asset, @{Label="sell_amount"; Expression={"{0,16:N4}" -f $_.sell_amount}}, sell_asset, @{Label="unit price (limit)"; Expression={"{0,16:N8}" -f $_.limit}}, limit_asset, @{Label="fee_amount"; Expression={"{0,16:N8}" -f $_.fee_amount}}, fee_asset, exchange, group, comment, id | Out-GridView -Title "cryptoanals: ur ledger"
    } else {
        $LedgerObject | Format-Table date, type, @{Label="buy_amount"; Expression={"{0,16:N4}" -f $_.buy_amount}}, buy_asset, @{Label="sell_amount"; Expression={"{0,16:N4}" -f $_.sell_amount}}, sell_asset, @{Label="unit price (limit)"; Expression={"{0,16:N8}" -f $_.limit}}, limit_asset, @{Label="fee_amount"; Expression={"{0,16:N8}" -f $_.fee_amount}}, fee_asset, exchange, group, comment, id
    }
}

<#
    Reimport and update the ledgers.
#>
function Update-Ledger {

    Write-Host " Update-Ledger " -ForegroundColor White -BackgroundColor Blue

    Write-Host "- Importing Kraken activities..." -NoNewline
    $kraken = Import-Trades -Kraken -File $my_kraken
    Write-Host " done!" -ForegroundColor Green

    Write-Host "- Importing Bittrex activities..." -NoNewline
    $bittrex = Import-Trades -Bittrex -File $my_bittrex
    Write-Host " done!" -ForegroundColor Green

    Write-Host "- Importing custom activities..." -NoNewline
    $customledger = Import-Trades -MyLedger -File $my_ledger
    Write-Host " done!" -ForegroundColor Green

    Write-Host "- Consolidating all activities..." -NoNewline
    $trades = ($kraken + $bittrex + $customledger) | Sort-Object date
    Write-Host " done!" -ForegroundColor Green

    
    
    Write-Host "- Exporting global variables `$kraken, `$bittrex and `$trades..." -NoNewline
    $global:kraken = $kraken;
    $global:bittrex = $bittrex;
    $global:trades = $trades;
    Write-Host " done!" -ForegroundColor Green
}

function Show-Wallet {
    param (
        [System.Collections.Generic.List[System.Object]]$Trades
    )

    Measure-Trades $Trades -Wallet -GUI
}